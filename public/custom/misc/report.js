$(document).ready(function(){

	// Funcs
	function renderUsers(data_user){
		var html = ""
		for(var a in data_user["users"].data){
			html += "<tr>"
			html += "<td>" + a + "</td>"
			html += "</tr>"
		}
		$("tbody.users").html(html)
	}

	function getUserList(){
		$.ajax({
			type: "GET",
			url: "/report/list_user"
		}).done(function(result){
			renderUsers(result)
		})
	}

	// Event
	$('.modal-trigger').leanModal({
		ready: function(){
			getUserList()
		}
	});
	$("button#btn-report").click(function(event){
	})
})