// Iki lha kuk yo harus ada yak?
var App = React.createClass({
	getInitialState: function(){
		return{
			baseAppUrl			: "/surat",
			currentPage			: 1,
			dataSuratMasuk	: [],
			myName					:	"",
			myNamaJabatan		: "",
		}
	},
	componentWillMount: function(){
		this.getDataSuratMasuk(this.state.currentPage)
	},
	getDataSuratMasuk: function(page){
		var _this = this
		this.setState({showPreloader: "block"})
		$.getJSON(this.state.baseAppUrl + "/suratmasuk/page/" + page, function(result){
			_this.setState({dataSuratMasuk: result["listsurat"]})
			_this.setState({showPreloader: "none"})
			_this.setState({myName: result["my_nama"]})
			_this.setState({myNamaJabatan: result["my_namajabatan"]})
			// Materialize.toast('Done', 1000, 'mytoast')
		})
	},
	componentDidMount: function(){
		// var _this = this
		// // Cell di klik
		// $("td.SPTCell").click(function(){
		// 	var masa_pajak  = $(this).attr("data-column")
		// 	var jenis_pajak = $(this).attr("data-row")
		// 	_this.setState({masa_pajak_terpilih: masa_pajak})
		// 	_this.setState({jenis_pajak_terpilih: jenis_pajak})
		// 	$(".modal-trigger2").click()
		// })
	},
	// set_tahun_pajak_terpilih: function(value){
	// 	this.setState({tahun_pajak_terpilih: value})
	// },
	// set_dataSPT: function(value){
	// 	this.setState({dataSPT: value})
	// },
	render: function(){
		return(
			<div>
				<AppPreloaderAnimation showPreloader={this.state.showPreloader} />
				<div className="row row-mod1" style={{height: "20px"}}>
					<div className="col s11">
						<AppHeader myName={this.state.myName} myNamaJabatan={this.state.myNamaJabatan} />
					</div>
					<div className="col s1"></div>
				</div>
				<div className="row">
					<div className="col s12">
						<TabelPengawasan dataSuratMasuk={this.state.dataSuratMasuk}/>
						<AppModal />
					</div>
				</div>
			</div>
		)
	}
})

var AppHeader = React.createClass({
	render: function(){
		return(
			<div className="app-header">
				<strong>{this.props.myName}</strong>&nbsp;
				<span className="app-header-small-font">{this.props.myNamaJabatan}</span>
			</div>
		)
	}
})

var AppModal = React.createClass({
	getInitialState: function(){
		return{
		}
	},
	componentWillMount: function(){

	},
	post: function(){
		// var _this = this
		// var url  = "/appodjok/input"
		// var data = {}
		// data.tahun_pajak = React.findDOMNode(this.refs.tahun_pajak).value
		// data.masa_pajak  = React.findDOMNode(this.refs.masa_pajak).value
		// data.jenis_pajak = React.findDOMNode(this.refs.jenis_pajak).value
		// data.normal_pembetulan  = React.findDOMNode(this.refs.normal_pembetulan).value
		// data.tanggal_bayar      = React.findDOMNode(this.refs.tanggal_bayar).value
		// data.tanggal_lapor      = React.findDOMNode(this.refs.tanggal_lapor).value
		// data.kurang_lebih_bayar = React.findDOMNode(this.refs.kurang_lebih_bayar).value
		// data.nilai_spt   = React.findDOMNode(this.refs.nilai_spt).value
		//
		// $.post(url, data, function(result){
		// 	var res = JSON.parse(result)
		// 	if(res["result"] == "OK"){
		// 		_this.props.getDataSPT(data.tahun_pajak)
		// 	}
		// })
	},
	componentDidMount: function(){
		// var _this = this
		// $('.modal-trigger2').leanModal({
		// 		ready: function(){
		// 		}
		// });
		// $('.datepicker').pickadate({
		// 	format: 'dd/mm/yyyy',
		// 	closeOnSelect: true,
		// });
	},

	render: function(){

		return (
			<div>
				<div id="window_form_search" className="modal modal-fixed-footer">
					<div className="modal-content">
						<div className="row">
							<div className="col s4">
								<div className="input-field">
									<label htmlFor="tanggal_lapor">Tanggal Lapor</label>
									<input placeholder="" id="tanggal_lapor" type="date" className="datepicker" ref="tanggal_lapor" />
								</div>
							</div>
							<div className="col s4">
								<div className="input-field">
									<label htmlFor="tanggal_bayar">Tanggal Bayar</label>
									<input placeholder="" id="tanggal_bayar" type="date" className="datepicker" ref="tanggal_bayar" />
								</div>
							</div>
						</div>
						<div className="row">
							<div className="col s4">
								<label htmlFor="kurang_lebih_bayar">Kurang/Lebih Bayar</label>
								<select id="kurang_lebih_bayar" ref="kurang_lebih_bayar" className="browser-default">
									<option	value="KB">Kurang Bayar</option>
									<option	value="LB">Lebih Bayar</option>
								</select>
							</div>
							<div className="col s8">
								<div className="input-field">
									<label htmlFor="nilai_spt">Nilai SPT</label>
									<input placeholder="" className="tooltipped" data-position="bottom" data-tooltip="Masukkan angka tanpa format" type="text" id="nilai_spt" ref="nilai_spt" />
								</div>
							</div>
						</div>
					</div>
					<div className="modal-footer">
						<a className="btn light-green accent-4" style={{marginRight:"2px"}} onClick={this.post}>Apply</a>
					</div>
				</div>
				<div className="menu-button" style={{display:"none"}}>
					<a className="btn-floating waves-effect waves-light blue darken-2 modal-trigger2 tooltipped" data-position="top" data-tooltip="Cari Surat" data-delay="5" data-target="window_form_search"><i className="material-icons">search</i></a>
				</div>
			</div>
		)
	}
})

var AppPreloaderAnimation = React.createClass({
	render: function() {
		return(
			<div className="progress zTop" style={{display: this.props.showPreloader}}>
		      <div className="indeterminate"></div>
		  </div>
		)
	}
})

var TabelPengawasan = React.createClass({
	getInitialState: function() {
		return {
		}
	},
	componentWillMount: function(){

	},
	render: function(){
			var _this = this
			return(
			<table className="table bordered striped">
				<thead>
					<tr>
						<th className="small">Agenda</th>
						<th className="small">Atasan</th>
						<th className="small">Diterima</th>
						<th className="middle">Asal</th>
						<th className="middle">Nomor</th>
						<th className="small">Tanggal</th>
						<th className="wide">Perihal</th>
						<th className="middle">Status</th>
						<th className="middle">File</th>
					</tr>
				</thead>
				<tbody>
					{
						this.props.dataSuratMasuk.map(function(surat){
								return(
									<tr>
										<td className="small">{surat.to_nomoragenda}</td>
										<td className="small">{surat.createdby_time}</td>
										<td className="small">{surat.to_tanggalditerima}</td>
										<td className="middle">{surat.asal}</td>
										<td className="middle">{surat.nomor}</td>
										<td className="small">{surat.tanggal}</td>
										<td className="wide" title={surat.hal}>{surat.hal}</td>
										<td className="small wrap">
												{surat.tag.map(function(data){
													var title = data.to_namajabatan + "\n" + data.to_nama + ""
													var _color = data.badge_color
													var _class = "glyphicon " + data.badge
													return(
															<i className="tiny material-icons" title={title} style={{cursor: "pointer", color: _color, fontSize: 18}}>person</i>
														)
												})}
										</td>
										<td className="small">
											{surat.files.map(function(file){
												var path = "/files/" + file.nama_file
												return(
													<a href={path} target="_new">
														<i className="tiny material-icons">attach_file</i>
														</a>
												)
											})}
										</td>
									</tr>
								)
						})
					}
				</tbody>
			</table>
		)
	}
})

React.render(<App />, document.getElementById('react'));
