// VSC ** ..
var DataViewInfoSelectedRow = React.createClass({
  getInitialState: function(){
    return({
    })
  },
  render: function(){
    return(
      <div className="footer-section-inner">
        <div>Terpilih: {this.props.SelectedDocSeries.length}</div>
      </div>
    )
  }
})

var DataViewCount = React.createClass({
    render: function() {
      var start = this.props.CurrentPage * 10 - 9
      var end   = this.props.CurrentPage * 10

      if (end >= this.props.DocsCount){
        end = this.props.DocsCount
      }
      return(
        <div className="footer-section-inner" style={{paddingRight: "10px"}}>
          <div>{start} - {end} dari {this.props.DocsCount}</div>
        </div>
      )
    }
})

var DataViewNavigator = React.createClass({
  changeCurrentPage: function(){
    var page = $("#current_page_number").val()
    this.props.changeCurrentPage(page)
  },
  componentDidUpdate: function(prevProps, prevState){
    // console.log(prevProps.CurrentPage, this.props.CurrentPage)
    // CurrentPage Changed
    if(prevProps.CurrentPage !== this.props.CurrentPage){
      this.props.getData()
    }
  },
  prevPage: function(){
    var page = this.props.CurrentPage - 1
    this.props.changeCurrentPage(page)
  },
  nextPage: function(){
    var page = this.props.CurrentPage + 1
    this.props.changeCurrentPage(page)
  },
  render: function(){
    return(
      <div className="footer-section-inner">
        <span>
          <i className="material-icons navigator" onClick={this.prevPage}>keyboard_arrow_left</i>
        </span>
        <span>
          <input id="current_page_number" maxLength="2" type="text" value={this.props.CurrentPage} onChange={this.changeCurrentPage} />
        </span>
        <span>
          <i className="material-icons navigator" onClick={this.nextPage}>keyboard_arrow_right</i>
        </span>
      </div>
    )
  }
})
