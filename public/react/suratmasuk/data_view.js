function resizeDataView(){
  var innerHeight = window.innerHeight
  var TableHeight = innerHeight - 180 + "px"
  $("tbody").css("height", TableHeight)
  setTimeout(resizeColumnView, 1000)
}

function resizeColumnView(){
  // WINDOW
  // 20 adalah perkiraan lebar scroll
  var innerWidth  = window.innerWidth - 20
  $("table#data-view-table").css("width", innerWidth + "px")

  // DEFAULT
  var _nomor_agenda_column = 60
  var _tanggal_column = 80
  var _asal_column = 200
  var _status_column = 100

  // the magic is here, buat kolom dinamis
  var _perihal_column = innerWidth - (_nomor_agenda_column + (_tanggal_column * 3) + (_asal_column * 2) + _status_column) - 18

  // CALCULATE
  // 8 adalah jumlah kolom;
  // 50 adalah toleransi (tergantung selera saja, can be UP or DOWN silakan dicoba-coba saja)
  var nomor_agenda_column = ((innerWidth/8-50) < _nomor_agenda_column ? _nomor_agenda_column : _nomor_agenda_column)
  var tanggal_column = ((innerWidth/8-50) < _tanggal_column ? _tanggal_column : _tanggal_column)
  var asal_column = ((innerWidth/8-50) < _asal_column ? _asal_column : _asal_column)
  var perihal_column = ((innerWidth/8-50) < _perihal_column ? _perihal_column : _perihal_column)
  var status_column = ((innerWidth/8-50) < _status_column ? _status_column : _status_column)

  // SET
  $(".nomor_agenda_column").css("width", nomor_agenda_column + "px")
  $(".tanggal_column").css("width", tanggal_column + "px")
  $(".asal_column").css("width", asal_column + "px")
  $(".perihal_column").css("width", perihal_column + "px")
  $(".status_column").css("width", status_column + "px")

  // MISC
  // console.log(innerWidth + "px")
}

var addEvent = function(object, type, callback) {
    if (object == null || typeof(object) == 'undefined') return;
    if (object.addEventListener) {
        object.addEventListener(type, callback, false);
    } else if (object.attachEvent) {
        object.attachEvent("on" + type, callback);
    } else {
        object["on"+type] = callback;
    }
};

var DataView_peoples = React.createClass({displayName: "DataView_peoples",
  render: function(){
    return(
      React.createElement("div", {className: "data-view-status-child"}, 
        
          this.props.peoples.map(function(tag){
            var jumlah = ""
            if (tag.jumlah > 1){
              jumlah = tag.jumlah
            }
            return(
              React.createElement("div", {className: "data-view-status-child-items"}, 
                React.createElement("span", {className: "tag", style: {color: tag.color}, title: tag.peoples}, 
                  React.createElement("i", {className: "material-icons tag"}, "person"), jumlah
                )
              )
            )
          })
        
      )
    )
  }
})

var DataView_files = React.createClass({displayName: "DataView_files",
  render: function(){
    return(
      React.createElement("div", null, 
        
          this.props.files.map(function(file){
            var url = URL_fileUpload + file.nama_file
            return(
              React.createElement("span", null, 
                React.createElement("a", {href: url}, React.createElement("i", {className: "material-icons tag-small"}, "attach_file"))
              )
            )
          })
        
      )
    )
  }
})

var DataView_arsip = React.createClass({displayName: "DataView_arsip",
  render: function(){
    return(
      React.createElement("div", null, 
        
          this.props.arsip.map(function(arsip){
            return(
              React.createElement("span", null, 
                React.createElement("i", {title: arsip, className: "material-icons tag-small", style: {color:'#64DD17'}}, "folder")
              )
            )
          })
        
      )
    )
  }
})

var DataView = React.createClass({displayName: "DataView",
  getInitialState: function() {
    return({

    })
  },
  componentWillMount: function(){
    // resizeDataView()
  },
  componentDidMount: function(){
    resizeDataView()
    addEvent(window, "resize", function(event) {
      resizeDataView()
    });
  },
  toTitleCase: function (str){
      var cantTouchThis = this.props.Singkatan
      return str.replace(/\w[\S.]*/g, function(txt){return cantTouchThis[txt.toUpperCase()] || txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
  },
  selectRowHandler: function(doc_series) {
    this.props.changeSelectedDocSeries(doc_series)
  },
  dblClickRowHandler: function(){
    $.each($("tr"), function(){
      $(this).click()
    })
  },
  render: function(){
    var _this = this
    return(
      React.createElement("div", {className: "main-container"}, 
        React.createElement("table", {style: {height: this.state.TableHeight}, className: "bordered", id: "data-view-table"}, 
          React.createElement("thead", {className: "data-view"}, 
            React.createElement("tr", null, 
              React.createElement("th", {className: "nomor_agenda_column"}, "AGENDA"), 
              React.createElement("th", {className: "tanggal_column"}, "ATASAN"), 
              React.createElement("th", {className: "tanggal_column"}, "DITERIMA"), 
              React.createElement("th", {className: "asal_column"}, "ASAL"), 
              React.createElement("th", {className: "asal_column"}, "NOMOR"), 
              React.createElement("th", {className: "tanggal_column"}, "TANGGAL"), 
              React.createElement("th", {className: "perihal_column"}, "PERIHAL"), 
              React.createElement("th", {className: "status_column"}, "STATUS")
            )
          ), 
          React.createElement("tbody", {className: "data-view"}, 
            this.props.Data_DataView.map(function(data){
              if(_this.props.SelectedDocSeries.indexOf(data.doc_series) > -1){
                var tr_class = "selected"
              }else{
                var tr_class = ""
              }

              if(!data.arsip){
                data.arsip = []
              }

              if(!data.files){
                data.files = []
              }

              return(
                React.createElement("tr", {onClick: _this.selectRowHandler.bind(this, data.doc_series), onDoubleClick: _this.dblClickRowHandler, className: tr_class}, 
                  React.createElement("td", {className: "nomor_agenda_column"}, data.to_nomoragenda), 
                  React.createElement("td", {className: "tanggal_column"}, data.createdby_time), 
                  React.createElement("td", {className: "tanggal_column"}, data.to_tanggalditerima), 
                  React.createElement("td", {className: "asal_column"}, _this.toTitleCase(data.asal)), 
                  React.createElement("td", {className: "asal_column"}, data.nomor), 
                  React.createElement("td", {className: "tanggal_column"}, data.tanggal), 
                  React.createElement("td", {className: "perihal_column"}, _this.toTitleCase(data.hal)), 
                  React.createElement("td", {className: "status_column"}, 
                    React.createElement(DataView_peoples, {peoples: data.tag_simplify}), 
                    React.createElement(DataView_files, {files: data.files}), 
                    React.createElement(DataView_arsip, {arsip: data.arsip})
                  )
                )
              )
            })
          )
        )
      )
    )
  }
})