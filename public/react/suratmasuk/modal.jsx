//
var ModalInput = React.createClass({
	getInitialState: function(){
		return({
			Hal_surat : "",
		})
	},
	componentDidMount: function(){
		var _this = this
		// Modal
		$("#modal_input").modal({
      dismissible: true,
			opacity: .1,
			in_duration: 100,
			out_duration: 100,
			complete: function() {
				_this.closeModal()
			}
    })
		// Datepicker
		$(".datepicker").pickadate({
			closeOnSelect: true,
			format: 'dd/mm/yyyy',
			selectYears: true,
			selectMonths: true,
			selectYears: 3,
			close: "",
		})
		// Autocomplete
		// **** asal surat
		$('.suratmasuk-asalsurat-autocomplete').autocomplete({
			source: function( request, response ) {
				$.ajax( {
				  url: URL_suratmasukSuggest + $("#FormInput_asalsurat").val(),
				  success: function( data ) {
						var json = JSON.parse(data)
				    response( json.result );
				  }
				} );
	    },
			select: function( event, ui ) {
				return false;
	    },
			focus: function( event, ui ) {
				$( "#FormInput_asalsurat" ).val( ui.item.word );
				return false;
	    },
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append( "<div>" + item.word + "</div>" )
        .appendTo( ul );
    };
	},
	componentDidUpdate: function(prevProps, prevState){
    if(prevProps.ModalInputIsOpen !== this.props.ModalInputIsOpen){
      if(this.props.ModalInputIsOpen){
        // ModalInputIsOpen is true
        $('#modal_input').modal('open');
      }else{
        // ModalInputIsOpen is false
        $('#modal_input').modal('close');
      }
    }
  },
	post: function(){
		var _this = this
		var url   = URL_suratmasukInput
		var data  = {}
		$.each($(".form-input"), function(){
			var fieldname   = $(this).attr("data-name")
			var value       = $(this).val()
			data[fieldname] = value
		})
		$.post(url, data, function(result){
			_this.props.getData(_this.props.CurrentPage)
			Materialize.toast(data.nomor + ' dari ' + data.asal, 3000, 'rounded')
		})
	},
	toTitleCase: function (str){
			var cantTouchThis = this.props.Singkatan
			return str.replace(/\w[\S.]*/g, function(txt){return cantTouchThis[txt.toUpperCase()] || txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
	},
	changeHalSurat: function(){
		var hal_value = $("#FormInput_perihalsurat").val()
		var hal       = this.toTitleCase(hal_value)
		this.setState({Hal_surat: hal})
	},
	resetForm: function(){
		$.each($(".form-input"), function(){
			$(this).val("")
		})
		this.setState({Hal_surat: ""})
	},
	closeModal: function() {
		this.props.changeModalInputIsOpen(false)
	},
	render: function(){
		return(
			<div>
			  <div id="modal_input" className="modal modal-fixed-footer">
			    <div className="modal-content">
						<div className="row">
							<div className="input-field col s6">
								<input placeholder="" id="FormInput_tanggalditerima" type="text" className="form-input datepicker" data-name="tanggal_diterima" />
								<label for="FormInput_tanggalditerima">Tgl. Diterima</label>
							</div>
						</div>
						<div className="row">
							<div className="input-field col s12">
								<input placeholder="" id="FormInput_asalsurat" type="text" className="form-input suratmasuk-asalsurat-autocomplete" data-name="asal" />
								<label for="FormInput_asalsurat">Asal Surat</label>
							</div>
						</div>
						<div className="row">
							<div className="input-field col s8">
								<input placeholder="" id="FormInput_nomorsurat" type="text" className="form-input" data-name="nomor" />
								<label for="FormInput_nomorsurat">Nomor Surat</label>
							</div>
							<div className="input-field col s4">
								<input placeholder="" id="FormInput_tanggalsurat" type="text" className="form-input datepicker" data-name="tanggal" />
								<label for="FormInput_tanggalsurat">Tgl. Surat</label>
							</div>
						</div>
			      <div className="row">
							<div className="input-field col s12">
								<textarea placeholder=""
									id="FormInput_perihalsurat"
									className="materialize-textarea form-input" data-name="hal"
									onChange={this.changeHalSurat} value={this.state.Hal_surat} />
								<label for="FormInput_perihalsurat">Perihal Surat</label>
							</div>
						</div>
			    </div>
			    <div className="modal-footer modal-footer-flex-end">
						<div>
				      <a className="modal-action waves-effect waves-black green btn" onClick={this.post} style={{marginLeft:"5px"}}>Input</a>
							<a className="modal-action waves-effect waves-black orange btn" onClick={this.resetForm}>Reset</a>
						</div>
			    </div>
			  </div>
			</div>
		)
	}
})

var ModalEdit = React.createClass({
	componentDidMount: function(){
		var _this = this
		// Modal
		$("#modal_edit").modal({
      dismissible: true,
			opacity: .1,
			in_duration: 100,
			out_duration: 100,
			complete: function() {
				_this.closeModal()
			}
    })
	},
	componentDidUpdate: function(prevProps, prevState){
    if(prevProps.ModalEditIsOpen !== this.props.ModalEditIsOpen){
      if(this.props.ModalEditIsOpen){
        // ModalInputIsOpen is true
        $('#modal_edit').modal('open');
				var url = URL_suratmasukDetailForEdit + this.props.SelectedDocSeries[0]
				$.getJSON(url, function(response){
					var result = response["datasurat"]
					$("#FormEdit_asalsurat").val(result.asal)
					$("#FormEdit_nomorsurat").val(result.nomor)
					$("#FormEdit_tanggalsurat").val(tanggalFormatterToIndonesian(result.tanggal))
					$("#FormEdit_perihalsurat").val(result.hal)
				})
      }else{
        // ModalInputIsOpen is false
        $('#modal_edit').modal('close');
      }
    }
  },
	post: function(){

		var _this = this
		var url   = URL_suratmasukEdit + this.props.SelectedDocSeries[0]
		var data  = {}

		$.each($(".form-edit"), function(){
			var fieldname   = $(this).attr("data-name")
			var value       = $(this).val()
			data[fieldname] = value
		})

		$.post(url, data, function(res){
			if(res.result == 'OK'){
				_this.props.getData(_this.props.CurrentPage)
				_this.closeModal()
			}
		})
	},
	closeModal: function() {
		this.props.changeModalEditIsOpen(false)
	},
	render: function(){
		return(
			<div>
			  <div id="modal_edit" className="modal modal-fixed-footer">
			    <div className="modal-content">
						<div className="row">
							<div className="input-field col s12">
								<input placeholder="" id="FormEdit_asalsurat" type="text" className="form-edit" data-name="asal" />
								<label for="FormEdit_asalsurat">Asal Surat</label>
							</div>
						</div>
						<div className="row">
							<div className="input-field col s8">
								<input placeholder="" id="FormEdit_nomorsurat" type="text" className="form-edit" data-name="nomor" />
								<label for="FormEdit_nomorsurat">Nomor Surat</label>
							</div>
							<div className="input-field col s4">
								<input placeholder="" id="FormEdit_tanggalsurat" type="text" className="form-edit" data-name="tanggal" />
								<label for="FormEdit_tanggalsurat">Tgl. Surat</label>
							</div>
						</div>
			      <div className="row">
							<div className="input-field col s12">
								<textarea placeholder="" id="FormEdit_perihalsurat" className="materialize-textarea form-edit" data-name="hal"></textarea>
								<label for="FormEdit_perihalsurat">Perihal Surat</label>
							</div>
						</div>
			    </div>
			    <div className="modal-footer modal-footer-flex-end">
						<div>
				      <a className="modal-action waves-effect waves-black green btn" onClick={this.post} style={{marginLeft:"5px"}}>Edit</a>
						</div>
			    </div>
			  </div>
			</div>
		)
	}
})

var ModalCari = React.createClass({
	componentDidMount: function(){
		var _this = this
		$("#modal_cari").modal({
      dismissible: true,
			opacity: .3,
			in_duration: 100,
			out_duration: 100,
			complete: function() {
				_this.closeModal()
			}
    })
	},
	componentDidUpdate: function(prevProps, prevState){
    if(prevProps.ModalCariIsOpen !== this.props.ModalCariIsOpen){
      if(this.props.ModalCariIsOpen){
        // ModalCariIsOpen is true
        $('#modal_cari').modal('open');
				var url = URL_suratmasukSearchData
				$.getJSON(url, function(response){
					$("#FormCari_nomoragenda").val(response["search-nomoragenda"])
					$("#FormCari_tanggalagenda").val(response["search-waktuterimasurat"])
					$("#FormCari_asalsurat").val(response["search-asal"])
					$("#FormCari_nomorsurat").val(response["search-nomorsurat"])
					$("#FormCari_tanggalsurat").val(response["search-tanggalsurat"])
					$("#FormCari_perihalsurat").val(response["search-hal"])
					$("#FormCari_disposisike").val(response["search-kodejabatan"])
					// belum disposisi
					if(response["search-belumdisposisi"] === "true"){
						$("#FormCari_belumdisposisi").prop("checked", true)
					}else if(response["search-belumdisposisi"] === ""){
						$("#FormCari_belumdisposisi").prop("checked", false)
					}
					// tanpa file
					if(response["search-tanpafile"] === "true"){
						$("#FormCari_tanpafile").prop("checked", true)
					}else if(response["search-belumdisposisi"] === ""){
						$("#FormCari_tanpafile").prop("checked", false)
					}
				})
      }else{
        // ModalCariIsOpen is false
        $('#modal_cari').modal('close');
      }
    }
  },
	closeModal: function() {
		this.props.changeModalCariIsOpen(false)
	},
	submitForm: function(){
		var _this = this
		var data  = {}
		$.each($(".form-cari"), function(){
			var fieldname = $(this).attr("data-name")
			var value     = $(this).val()
			if(fieldname === "belum_disposisi" || fieldname === "tanpa_file"){
				if($(this).prop("checked") === true){
					value = "true"
				}else if($(this).prop("checked") === false){
					value = ""
				}

			}
			data[fieldname] = value
		})
		var url = URL_suratmasukSearch
		$.post(url, data, function(response){
			if(response.result === "OK"){
				_this.props.getData()
			}
		})
	},
	clearSearch: function(){
		var _this = this
		var url   = URL_suratmasukSearchClear
		$.getJSON(url, function(response){
			if(response.result === "OK"){
				_this.props.getData()
			}
		})
	},
	render: function(){
		return(
			<div>
			  <div id="modal_cari" className="modal modal-fixed-footer">
			    <div className="modal-content">
						<div className="row">
							<div className="input-field col s6">
								<input placeholder="" id="FormCari_nomoragenda" type="text" className="form-cari" data-name="nomor_agenda"/>
								<label for="FormCari_nomoragenda">Nomor Agenda</label>
							</div>
							<div className="input-field col s6">
								<input placeholder="" id="FormCari_tanggalagenda" type="text" className="form-cari" data-name="waktuterima_surat" />
								<label for="FormCari_tanggalagenda">Tgl. Diterima</label>
							</div>
						</div>
						<div className="row">
							<div className="input-field col s12">
								<input placeholder="" id="FormCari_asalsurat" type="text" className="form-cari" data-name="asal" />
								<label for="FormCari_asalsurat">Asal Surat</label>
							</div>
						</div>
						<div className="row">
							<div className="input-field col s8">
								<input placeholder="" id="FormCari_nomorsurat" type="text" className="form-cari" data-name="nomor_surat" />
								<label for="FormCari_nomorsurat">Nomor Surat</label>
							</div>
							<div className="input-field col s4">
								<input placeholder="" id="FormCari_tanggalsurat" type="text" className="form-cari" data-name="tanggal_surat" />
								<label for="FormCari_tanggalsurat">Tgl. Surat</label>
							</div>
						</div>
			      <div className="row">
							<div className="input-field col s8">
								<textarea placeholder="" id="FormCari_perihalsurat" className="materialize-textarea form-cari" data-name="hal"></textarea>
								<label for="FormCari_perihalsurat">Perihal Surat</label>
							</div>
							<div className="input-field col s4">
								<input placeholder="" id="FormCari_disposisike" type="text" className="form-cari" data-name="kode_jabatan" />
								<label for="FormCari_disposisike">Disposisi ke</label>
							</div>
						</div>
			    </div>
			    <div className="modal-footer modal-footer-flex">
						<div>
							<div className="switch">
								<label>
									Belum disposisi
									<input id="FormCari_belumdisposisi" type="checkbox" className="form-cari" data-name="belum_disposisi" onChange={this.submitForm}/>
									<span className="lever"></span>
								</label>
							</div>
						</div>
						<div>
							<div className="switch">
								<label>
									Tanpa file
									<input id="FormCari_tanpafile" type="checkbox" className="form-cari" data-name="tanpa_file" onChange={this.submitForm}/>
									<span className="lever"></span>
								</label>
							</div>
						</div>
						<div>
				      <a className="modal-action modal-close waves-effect waves-black green btn" onClick={this.submitForm} style={{marginLeft:"5px"}}>Cari</a>
							<a className="modal-action modal-close waves-effect waves-black orange btn" onClick={this.clearSearch}>Clear</a>
						</div>
			    </div>
			  </div>
			</div>
		)
	}
})

var ModalDisposisi = React.createClass({
	getInitialState: function(){
		return({
			ListDisposisi : [
				{name: 'dis_untukdiketahui',label: 'Untuk Diketahui'},
				{name: 'dis_untukperhatian',label: 'Untuk Perhatian'},
				{name: 'dis_teliti',label: 'Teliti & Pendapat'},
				{name: 'dis_selesai',label: 'Selesaikan'},
				{name: 'dis_perbaiki',label: 'Perbaiki'},
				{name: 'dis_bicarakan',label: 'Bicarakan dengan saya'},
				{name: 'dis_sesuaicatatan',label: 'Sesuai Catatan'},
				{name: 'dis_perbanyak',label: 'Perbanyak ... kali,asli ke ...'},
				{name: 'dis_setuju',label: 'Setuju'},
				{name: 'dis_tolak',label: 'Tolak'},
				{name: 'dis_edarkan',label: 'Edarkan'},
				{name: 'dis_jawab',label: 'Jawab'},
				{name: 'dis_ingatkan',label: 'Ingatkan'},
				{name: 'dis_simpan',label: 'Simpan'},
				{name: 'blank',label: '...'}
			],
			bulk_kodejabatan: [],
			bulk_disposisi: [],
			net_pic_list: [],
			pic_list: [],
			is_tgl_terima_setted: false,
		})
	},
	componentDidMount: function(){
		var _this = this
		// Modal
		$("#modal_diposisi").modal({
      dismissible: true,
			opacity: .1,
			in_duration: 100,
			out_duration: 100,
			complete: function() {
				_this.closeModal()
			}
    })
	},
	componentDidUpdate: function(prevProps, prevState){
		// console.log("prev ",prevProps.ModalDisposisiIsOpen, "props", this.props.ModalDisposisiIsOpen)
		var _this = this
    if(prevProps.ModalDisposisiIsOpen !== this.props.ModalDisposisiIsOpen){
      if(this.props.ModalDisposisiIsOpen){
        // ModalInputIsOpen is true
        $('#modal_diposisi').modal('open');
				// init funcs
				this.getData()
      }else{
        // ModalInputIsOpen is false
        $('#modal_disposisi').modal('close');
      }
    }
  },
	getData: function(){
		var _this = this
		var url = URL_suratmasukDetailForDisposisi + this.props.SelectedDocSeries[0]
		$.getJSON(url, function(res){
			var bawahan  = _this.props.ListBawahan
			var _bawahan = []
			var _pic     = []
			for(var a in bawahan){
				var _obj = {}
				var kode_bawahan         = bawahan[a].Kode_jabatan
				var nama_jabatan_bawahan = bawahan[a].Nama_jabatan
				var nama_jabatan_pendek_bawahan = bawahan[a].Nama_jabatan_pendek

				_obj.Kode_jabatan = kode_bawahan
				_obj.Nama_jabatan = nama_jabatan_bawahan
				_obj.Nama_jabatan_pendek = nama_jabatan_pendek_bawahan

				if(res.datasurat.tag[kode_bawahan] == undefined){
					_bawahan.push(_obj)
				}else{
					_pic.push(_obj)
				}
			}

			console.log()

			if(res.datasurat.tag[_this.props.MyKodeJabatan].to_tanggalditerima == undefined
				|| res.datasurat.tag[_this.props.MyKodeJabatan].to_tanggalditerima == ""
				// || res.datasurat.tag[_this.props.MyKodeJabatan].to_tanggalditerima == "-"
			){
				_this.setState({is_tgl_terima_setted: false})
			}else{
				_this.setState({is_tgl_terima_setted: true})
			}

			_this.setState({net_pic_list: _bawahan})
			_this.setState({pic_list: _pic})
		})
	},
	setTglTerima: function(){
		var _this = this
		var url   = URL_suratmasukSetTglTerima
		var data  = "doc_series=" + this.props.SelectedDocSeries[0]
		$.post(url, data, function(res){
			if(res.result == "OK"){
				_this.props.getData() // update list surat
				_this.getData() // update modal
			}
		})
	},
	removeTag: function(obj){
		// console.log(obj)
		var _this = this
		var url   = URL_suratmasukRemoveDisposisi + this.props.SelectedDocSeries[0]
		var data  = {}
		data.kode_jabatan = obj.Kode_jabatan
		$.post(url, data, function(result){
			if(result.result == "OK"){
				_this.getData()
				_this.props.getData()
			}
		})
	},
	post: function(){
		var _this = this
		var list_kodejabatan = this.state.bulk_kodejabatan
		for(var a in list_kodejabatan){
			var data = {}
			data.kode_jabatan = list_kodejabatan[a]
			data.disposisi    = JSON.stringify(this.state.bulk_disposisi)
			data.catatan	    = $("FormDisposisi_catatan").val()
			var url           = URL_suratmasukAddDisposisi + this.props.SelectedDocSeries[0]
			$.post(url, data, function(result){
				if(result.result == "OK"){
					_this.props.getData()
					// _this.hideWindow()
					_this.getData()
				}
			})
		}
	},
	closeModal: function() {
		this.props.changeModalDisposisiIsOpen(false)
	},
	addToKodeJabatanBulk: function(kode_jabatan){
		var prevState = this.state.bulk_kodejabatan
		var inArray = $.inArray(kode_jabatan, prevState)
		if(inArray == -1){
			prevState.push(kode_jabatan)
		}else{
			var index = prevState.indexOf(kode_jabatan)
			prevState.splice(index, 1)
		}
		this.setState({bulk_kodejabatan: prevState})
		// console.log(this.state.bulk_kodejabatan)
	},
	addToDisposisiBulk: function(disposisi){
		var inArray = $.inArray(disposisi, this.state.bulk_disposisi)
		if(inArray == -1){
			this.state.bulk_disposisi.push(disposisi)
		}else{
			var index = this.state.bulk_disposisi.indexOf(disposisi)
			this.state.bulk_disposisi.splice(index, 1)
		}
		// console.log(this.state.bulk_disposisi)
	},
	render: function(){
		var _this = this
		if(this.state.is_tgl_terima_setted){
			return(
				<div>
				  <div id="modal_diposisi" className="modal modal-fixed-footer">
				    <div className="modal-content">
							<div className="row">
								<div className="col s12 flex">
									{this.state.net_pic_list.map(function(bawahan){
										var id = bawahan.Kode_jabatan
										if($.inArray(bawahan.Kode_jabatan, _this.state.bulk_kodejabatan) == -1){
											var checked = false
										}else{
											var checked = true
										}
										return(
											<span className="badge">
												<input type="checkbox" className="filled-in" id={id} checked={checked} onClick={_this.addToKodeJabatanBulk.bind(this, bawahan.Kode_jabatan)} />
												<label className="tiny color-purple" htmlFor={id}>{bawahan.Nama_jabatan_pendek}</label>
											</span>
										)
									})}
								</div>
							</div>
							<div className="row">
								<div className="col s12 flex">
									{this.state.ListDisposisi.map(function(disposisi){
										var id = disposisi.name
										return(
											<span className="badge">
												<input type="checkbox" id={id} onClick={_this.addToDisposisiBulk.bind(this, disposisi)} />
												<label className="tiny color-green" htmlFor={id}>{disposisi.label}</label>
											</span>
										)
									})}
								</div>
							</div>
							<div className="row">
								<div className="col s12 flex">
									{this.state.pic_list.map(function(bawahan){
											return (
												<div className="chip custom-chip">
													<label>
														{bawahan.Nama_jabatan_pendek} <span onClick={_this.removeTag.bind(this, bawahan)}>X</span>
													</label>
												</div>
											)
									})}
								</div>
							</div>
							<div className="row">
								<div className="col s12 flex">
									<label for="FormDisposisi_catatan">Catatan</label>
									<textarea placeholder="" id="FormDisposisi_catatan" className="materialize-textarea" ref="catatan_disposisi"></textarea>
								</div>
							</div>
				    </div>
				    <div className="modal-footer modal-footer-flex-end">
							<div>
					      <a className="modal-action waves-effect waves-black green btn" onClick={this.post} style={{marginLeft:"5px"}}>Disposisi</a>
							</div>
				    </div>
				  </div>
				</div>
			)
		}else{
			return(
				<div>
				  <div id="modal_diposisi" className="modal modal-fixed-footer">
				    <div className="modal-content">
							<button className="btn light-green accent-4" onClick={this.setTglTerima} >Set Tanggal Terima</button>
						</div>
					</div>
				</div>
			)
		}
	}
})

var ModalArsip = React.createClass({
	getInitialState: function(){
		return({
			list_arsip : []
		})
	},
	componentDidMount: function(){
		var _this = this
		$("#modal_arsip").modal({
      dismissible: true,
			opacity: .3,
			in_duration: 100,
			out_duration: 100,
			complete: function() {
				_this.closeModal()
			}
    })
		// Autocomplete
		// **** Arsip
		$('.arsip-value-autocomplete').autocomplete({
			source: function( request, response ) {
				var key = $("#FormArsip_value").val()
				$.ajax( {
					url: URL_searchArsip + key,
					success: function( data ) {
						response(data.list)
					}
				} );
			},
			select: function( event, ui ) {
				return false;
			},
			focus: function( event, ui ) {
				$( "#FormArsip_value" ).val( ui.item.Keterangan );
				$( "#FormArsip_kodearsip" ).val( ui.item.Kode_arsip );
				return false;
			},
		}).autocomplete( "instance" )._renderItem = function( ul, item ) {
			return $( "<li>" )
				.append("<div>")
				.append("<div style='font-color:green;font-size:0.5em'>" + item.Kode_arsip + "</div>")
				.append("<div style='font-color:green;font-size:0.7em'>" + item.Keterangan + "</div>")
				.append("</div>" )
				.appendTo( ul );
		};
	},
	componentDidUpdate: function(prevProps, prevState){
    if(prevProps.ModalArsipIsOpen !== this.props.ModalArsipIsOpen){
			var _this = this
      if(this.props.ModalArsipIsOpen){
        // ModalArsipIsOpen is true
        $('#modal_arsip').modal('open');
				// Get suratmasuk detail
				var url = URL_suratmasukDetailForArsip + this.props.SelectedDocSeries[0]
				$.getJSON(url, function(response){
					console.log(response)
					// _this.setState({list_arsip: response.datasurat.arsip})
				})

				// Get list arsip
				this.getListArsip()
      }else{
        // ModalArsipIsOpen is false
        $('#modal_arsip').modal('close');
      }
    }
  },
	closeModal: function() {
		this.props.changeModalArsipIsOpen(false)
	},
	removeArsip: function(obj){
		var sure_to_delete = confirm("Yakin ingin menghapus?");
		if (sure_to_delete) {
				var _this = this
				var url   = URL_removeArsip + this.props.SelectedDocSeries[0]
				var data  = {}
				data.id_arsip       = obj.detail.id
				data.kode_arsip     = obj.detail.data.kode_arsip.replace(".","")
				$.post(url, data, function(result){
					// refresh list arsip
					_this.getListArsip()
					// refresh list surat masuk
					_this.props.getData(_this.props.CurrentPage)
				})
		}
	},
	getListArsip: function() {
		var _this = this
		var url   = URL_suratmasukDetailForArsip + this.props.SelectedDocSeries[0]
		$.getJSON(url, function(response){
			if(response.datasurat){
				_this.setState({list_arsip: response.datasurat})
			}else{
				_this.setState({list_arsip: []})
			}
		})
	},
	onSubmit: function() {
		var _this      = this
		var data       = {}
		var Ket_arsip  = $("#FormArsip_value").val()
		var Kode_arsip = $("#FormArsip_kodearsip").val()
		var Doc_series = this.props.SelectedDocSeries[0]
		data["kode_arsip"] = Kode_arsip
		data["keterangan"] = Ket_arsip
		$.post(URL_submitArsip + Doc_series, data, function(result){
			_this.props.getData(_this.props.CurrentPage)
			_this.getListArsip()
			Materialize.toast("OK", 3000, 'rounded')
		})
	},
	render: function(){
		var _this = this
		return(
			<div>
			  <div id="modal_arsip" className="modal modal-fixed-footer">
			    <div className="modal-content">
						<div className="row">
							<div className="input-field col s12">
								<textarea placeholder=""
									id="FormArsip_value"
									type="text"
									className="materialize-textarea form-input arsip-value-autocomplete"
									data-name="arsip_value"></textarea>
								<label for="FormArsip_value">Kode/Keterangan Arsip</label>
							</div>
						</div>
						<div className="row">
							<div className="col s9">
								<input type="text" id="FormArsip_kodearsip" disabled />
							</div>
							<div className="col s2">
								<a className="waves-effect waves-light btn" onClick={this.onSubmit}>Proses</a>
							</div>
						</div>
						<div className="row">
							{
								this.state.list_arsip.map(function(arsip){
									return(
										<div>
										<div>ARSIP</div>
										<div className="row list-arsip">
											{
												this.state.list_arsip.map(function(arsip){
													return(
														<ModalArsip_detail_arsip data={arsip} removeArsip={_this.removeArsip}/>
													)
												})
											}
										</div>
										</div>
									)}
								)
						}
						</div>
					</div>
				</div>
			</div>
		)}
})

var ModalArsip_detail_arsip = React.createClass({
	render: function(){
		var arsip = this.props.data
		return(
			<div className="list-arsip-item">
				<div>{arsip.detail.by_name}</div>
				<div style={{fontSize: "0.8em", color: "green"}}>{tanggalFormatterToIndonesian2(arsip.detail.waktu)}</div>
				<div style={{fontWeight: "bold"}}>{arsip.detail.data.keterangan}</div>
				<span onClick={this.props.removeArsip.bind(this, arsip)}>Remove</span>
			</div>
		)
	}
})

var ModalFile = React.createClass({
	getInitialState: function(){
		return({
			list_file: [],
		})
	},
	componentDidMount: function(){
		var _this = this
		$("#modal_file").modal({
      dismissible: true,
			opacity: .3,
			in_duration: 100,
			out_duration: 100,
			complete: function() {
				_this.closeModal()
			}
    });
		$('#upload-form').ajaxForm({
			beforeSend: function() {
					$("#upload-button").val("Upload")
			},
			uploadProgress: function(event, position, total, percentComplete) {
					var persen = percentComplete + "%"
					$("#upload-button").val(persen)
					$("#upload-loading-progress").css("width",persen)
			},
			success: function() {
				$("#upload-button").val("Success")
			},
			complete: function(xhr) {
				$("#upload-button").val("Completed")
				$("#upload-loading-progress").css("width","0%")
				Materialize.toast("Upload berhasil !", 3000, 'rounded')
				_this.props.getData(_this.props.CurrentPage)
			}
			});
	},
	componentDidUpdate: function(prevProps, prevState){
    if(prevProps.ModalFileIsOpen !== this.props.ModalFileIsOpen){
      if(this.props.ModalFileIsOpen){
        // ModalArsipIsOpen is true
        $('#modal_file').modal('open');
				// Get list arsip
				this.getListFile()
      }else{
        // ModalArsipIsOpen is false
        $('#modal_file').modal('close');
      }
    }
  },
	closeModal: function() {
		this.props.changeModalFileIsOpen(false)
	},
	getListFile: function() {
		var _this = this
		var url   = URL_suratmasukDetailForFile + this.props.SelectedDocSeries[0]
		$.getJSON(url, function(response){
			if(response.datasurat){
				_this.setState({list_file: response.datasurat})
			}else{
				_this.setState({list_file: []})
			}
			// console.log(response.datasurat.files)
		})
	},
	removeFile: function(obj){
		var sure_to_delete = confirm("Yakin ingin menghapus?");
		if (sure_to_delete) {
			var _this = this
			var url   = URL_suratmasukRemoveFile + obj.nama_file
			$.getJSON(url, function(response){
				// refresh list arsip
				console.log("Remove file :", response)
				_this.getListFile()
				// refresh list surat masuk
				_this.props.getData(_this.props.CurrentPage)
			})
		}
	},
	render: function(){
		var _this = this
		var URL   =  URL_suratmasukFile + this.props.SelectedDocSeries[0]
		return(
			<div>
			  <div id="modal_file" className="modal modal-fixed-footer">
			    <div className="modal-content">
						<div className="row">
							<form action={URL} encType="multipart/form-data" method="post" id="upload-form">
								<input type="file" name="file" id="file" /><br/><br/>
								<input className="btn btn-primary btn-xs" type="submit" name="submit" value="Upload" id="upload-button" />
							</form>
						</div>
						<div className="row">
							{
								this.state.list_file.map(function(file){
									return(
										<ModalFile_detail_file removeFile={_this.removeFile} data={file} />
									)
								})
							}
						</div>
			    </div>
			  </div>
			</div>
		)
	}
})

var ModalFile_detail_file = React.createClass({
	render: function(){
		var file = this.props.data
		var url = URL_fileUpload + file.nama_file
		return(
			<div className="list-arsip-item">
				<div>
					<strong>
						<a href={url}>{file.nama_file}</a>
					</strong>
				</div>
				<div style={{fontSize: "0.8em"}}>{tanggalFormatterToIndonesian2(file.waktu)}</div>
				<span onClick={this.props.removeFile.bind(this, file)}>Remove</span>
			</div>
		)
	}
})
