var Header = React.createClass({displayName: "Header",
  componentDidMount: function(){
    $(".button-collapse").sideNav();
  },
  render: function(){
    return(
      React.createElement("div", {className: "header"}, 
        React.createElement("div", {style: {paddingTop: "4px", marginRight: "24px"}}, 
          React.createElement("a", {href: "#", "data-activates": "slide-out", className: "button-collapse"}, React.createElement("i", {className: "material-icons"}, "menu"))
        ), 
        React.createElement("div", null, 
          React.createElement("font", {style: {fontWeight: "bold", fontSize: "1.2em"}}, this.props.HeaderAccountName), 
          React.createElement("span", null, " "), 
          React.createElement("font", {style: {fontSize: ".9em"}}, this.props.HeaderAccountJabatan)
        )
      )
    )
  }
})