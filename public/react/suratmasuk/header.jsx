//
var Header = React.createClass({
  componentDidMount: function(){
    $(".button-collapse").sideNav();
  },
  render: function(){
    return(
      <div className="header">
        <div style={{paddingTop: "4px", marginRight: "24px"}}>
          <a href="#" data-activates="slide-out" className="button-collapse"><i className="material-icons">menu</i></a>
        </div>
        <div>
          <font style={{fontWeight: "bold", fontSize: "1.2em"}}>{this.props.HeaderAccountName}</font>
          <span> </span>
          <font style={{fontSize: ".9em"}}>{this.props.HeaderAccountJabatan}</font>
        </div>
      </div>
    )
  }
})
