var MenuCari = React.createClass({displayName: "MenuCari",
  render: function(){
    if(this.props.SearchLength > 0){
      var _class = "btn-floating waves-effect waves-light red accent-3 overfly"
    }else{
      var _class = "btn-floating waves-effect waves-light blue accent-3 overfly"
    }
    return(
      React.createElement("a", {className: _class, onClick: this.props.toggleModalCariIsOpen}, 
        React.createElement("i", {className: "material-icons left"}, "search")
      )
    )
  }
})

var MenuInput = React.createClass({displayName: "MenuInput",

  render: function(){
    return(
      React.createElement("a", {className: "btn-floating waves-effect waves-light blue accent-3 overfly", onClick: this.props.toggleModalInputIsOpen, style: {marginLeft: "5px"}}, 
        React.createElement("i", {className: "material-icons left"}, "add")
      )
    )
  }
})

var MenuPrint = React.createClass({displayName: "MenuPrint",
  onClick: function(){
    var obj  = {"UniqCHar": "123ABC", "Data": this.props.SelectedDocSeries}
    var _obj = JSON.stringify(obj)
    window.open(URL_suratmasukPDFGenerator + _obj, "new_window")
  },
  render: function(){
    if(this.props.SelectedDocSeries.length >= 1){
      var _class = "btn-floating waves-effect waves-light blue accent-3 overfly"
    }else{
      var _class = "hidden"
    }
    return(
      React.createElement("a", {className: _class, style: {marginLeft: "5px"}, onClick: this.onClick}, 
        React.createElement("i", {className: "material-icons left"}, "print")
      )
    )
  }
})

var MenuEdit = React.createClass({displayName: "MenuEdit",

  render: function(){
    if(this.props.SelectedDocSeries.length == 1){
      var _class = "btn-floating waves-effect waves-light blue accent-3 overfly"
    }else{
      var _class = "hidden"
    }
    return(
      React.createElement("a", {className: _class, style: {marginLeft: "5px"}, onClick: this.props.toggleModalEditIsOpen}, 
        React.createElement("i", {className: "material-icons left"}, "mode_edit")
      )
    )
  }
})

var MenuDisposisi = React.createClass({displayName: "MenuDisposisi",

  render: function(){
    if(this.props.SelectedDocSeries.length == 1){
      var _class = "btn-floating waves-effect waves-light blue accent-3 overfly"
    }else{
      var _class = "hidden"
    }
    return(
      React.createElement("a", {className: _class, style: {marginLeft: "5px"}, onClick: this.props.toggleModalDisposisiIsOpen}, 
        React.createElement("i", {className: "material-icons left"}, "people")
      )
    )
  }
})

var MenuArsip = React.createClass({displayName: "MenuArsip",

  render: function(){
    if(this.props.SelectedDocSeries.length == 1){
      var _class = "btn-floating waves-effect waves-light blue accent-3 overfly"
    }else{
      var _class = "hidden"
    }
    return(
      React.createElement("a", {className: _class, style: {marginLeft: "5px"}, onClick: this.props.toggleModalArsipIsOpen}, 
        React.createElement("i", {className: "material-icons left"}, "folder")
      )
    )
  }
})

var MenuFile = React.createClass({displayName: "MenuFile",

  render: function(){
    if(this.props.SelectedDocSeries.length == 1){
      var _class = "btn-floating waves-effect waves-light blue accent-3 overfly"
    }else{
      var _class = "hidden"
    }
    return(
      React.createElement("a", {className: _class, style: {marginLeft: "5px"}, onClick: this.props.toggleModalFileIsOpen}, 
        React.createElement("i", {className: "material-icons left"}, "attach_file")
      )
    )
  }
})

var MenuInfo = React.createClass({displayName: "MenuInfo",

  render: function(){
    if(this.props.SelectedDocSeries.length == 1){
      var _class = "btn-floating waves-effect waves-light blue accent-3 overfly"
    }else{
      var _class = "hidden"
    }
    return(
      React.createElement("a", {className: _class, style: {marginLeft: "5px"}}, 
        React.createElement("i", {className: "material-icons left"}, "info_outline")
      )
    )
  }
})