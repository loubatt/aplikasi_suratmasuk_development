var URL_suratmasukPage                = '/surat/suratmasuk/page/'
var URL_suratmasukSearch              = '/surat/suratmasuk/search'
var URL_suratmasukSearchData          = '/surat/suratmasuk/search/getvalue'
var URL_suratmasukSearchClear         = '/surat/suratmasuk/search/clear'
var URL_suratmasukSuggest			        = '/surat/suratmasuk/suggest/'
var URL_suratmasukInput			          = '/surat/suratmasuk/input'
var URL_suratmasukDetailForEdit	      = '/surat/suratmasuk/detail/for_edit/'
var URL_suratmasukDetailForDisposisi	= '/surat/suratmasuk/detail/for_disposisi/'
var URL_suratmasukDetailForArsip      = '/surat/suratmasuk/detail/for_arsip/'
var URL_suratmasukDetailForFile       = '/surat/suratmasuk/file/list/'
var URL_suratmasukEdit			          = '/surat/suratmasuk/edit/'
var URL_suratmasukAddDisposisi        = '/surat/suratmasuk/pic/add/'
var URL_suratmasukRemoveDisposisi     = '/surat/suratmasuk/pic/remove/'
var URL_suratmasukPDFGenerator        = '/surat/suratmasuk/pdf/'
var URL_suratmasukFile        			  = '/surat/suratmasuk/file/upload/'
var URL_suratmasukRemoveFile   			  = '/surat/suratmasuk/file/remove/'
var URL_suratmasukSetTglTerima 			  = '/surat/suratmasuk/settanggalterima'
var URL_singkatan 						        = '/surat/singkatan'
var URL_listBawahan                   = '/surat/jabatan/bawahan'
var URL_searchArsip                   = '/surat/arsip/kodeorketerangan/'
var URL_submitArsip                   = '/surat/arsip/sms/'
var URL_removeArsip										= '/surat/arsip/remove/sms/'
var URL_fileUpload                    = '/files/'

var libBulanIndonesia = {
		"01": {"bulan_pendek": "Jan", "bulan_panjang": "Januari"},
		"02": {"bulan_pendek": "Feb", "bulan_panjang": "Februari"},
		"03": {"bulan_pendek": "Mar", "bulan_panjang": "Maret"},
		"04": {"bulan_pendek": "Apr", "bulan_panjang": "April"},
		"05": {"bulan_pendek": "Mei", "bulan_panjang": "Mei"},
		"06": {"bulan_pendek": "Jun", "bulan_panjang": "Juni"},
		"07": {"bulan_pendek": "Jul", "bulan_panjang": "Juli"},
		"08": {"bulan_pendek": "Ags", "bulan_panjang": "Agustus"},
		"09": {"bulan_pendek": "Sep", "bulan_panjang": "September"},
		"10": {"bulan_pendek": "Okt", "bulan_panjang": "Oktober"},
		"11": {"bulan_pendek": "Nov", "bulan_panjang": "November"},
		"12": {"bulan_pendek": "Des", "bulan_panjang": "Desember"}
	}

var tanggalFormatterToIndonesian = function(tgl){
	// 2015-11-12 -> 12/11/2015
	if(tgl != undefined && tgl.length == 10){
		var tgl_split = tgl.split("-")
		return tgl_split[2] + "/" + tgl_split[1] + "/" + tgl_split[0]
	}else{
		return "-"
	}
}

var tanggalFormatterToIndonesian2 = function(tgl){
	// 2017-04-06 11:43:35.2376017 +0800 SGT -> 06 Apr 2016 11:43
	if(tgl != undefined && tgl.length > 15){
		var tanggal     = tgl.split(" ")[0]
		var _jam        = tgl.split(" ")[1].split(".")[0].split(":")
		var tgl_split   = tanggal.split("-")
		var bulan_angka = tgl_split[1]
		return tgl_split[2] + " " + libBulanIndonesia[bulan_angka].bulan_pendek + " " + tgl_split[0] + " " + _jam[0] + ":" + _jam[1]
	}else{
		return "-"
	}
}

var App = React.createClass({displayName: "App",
	getInitialState: function(){
		return({
			ModalInputIsOpen: false,
			ModalEditIsOpen: false,
			ModalCariIsOpen: false,
      ModalDisposisiIsOpen: false,
      ModalArsipIsOpen: false,
      ModalFileIsOpen: false,
			MyKodeJabatan: "",
			Data_DataView: [],
			DocsCount: 0,
			CurrentPage: 1,
			HeaderAccountName: "",
			HeaderAccountJabatan: "",
			SelectedDocSeries: [],
			SearchLength: 0,
			Singkatan: {},
      ListBawahan: [],
		})
	},
	changeSelectedDocSeries: function(doc_series){
		var list  = this.state.SelectedDocSeries
		var index = list.indexOf(doc_series)
		if(index === -1){
			list.push(doc_series)
		}else{
			list.splice(index, 1)
		}
		this.setState({SelectedDocSeries: list})
	},
	componentDidMount: function(){
		this.getSingkatan()
		this.getData()
    this.getListBawahan()
	},
	getSingkatan: function(){
		var _this = this
		var url   = URL_singkatan
		$.getJSON(url, function(response){
			_this.setState({Singkatan: response.list})
		})
	},
	getData: function(){
		var _this = this
		var url   = URL_suratmasukPage + this.state.CurrentPage
		$.getJSON(url, function(response){
			if(response.listsurat){
				_this.setState({Data_DataView: response.listsurat})
			}else{
				_this.setState({Data_DataView: []})
				_this.setState({CurrentPage: 1})
			}
			_this.setState({DocsCount: response.count})
			_this.setState({HeaderAccountName: response.my_nama})
			_this.setState({HeaderAccountJabatan: response.my_namajabatan})
			_this.setState({SearchLength: response.search_length})
			_this.setState({MyKodeJabatan: response.my_kodejabatan})
		})
	},
  getListBawahan: function(){
    var _this = this
    var url   = URL_listBawahan
    $.getJSON(url, function(result){
      _this.setState({ListBawahan: result.listbawahan})
    })
  },
	changeCurrentPage: function(_page){
		var page = parseInt(_page)
		if(page){
			if(page < 1){
				page = 1
			}
			this.setState({CurrentPage: page})
		}else{
			page = this.state.CurrentPage
		}
	},
	changeModalCariIsOpen: function(value) {
		this.setState({ModalCariIsOpen: value})
	},
	changeModalInputIsOpen: function(value) {
		this.setState({ModalInputIsOpen: value})
	},
  changeModalEditIsOpen: function(value) {
		this.setState({ModalEditIsOpen: value})
	},
  changeModalDisposisiIsOpen: function(value) {
    this.setState({ModalDisposisiIsOpen: value})
  },
	changeModalArsipIsOpen: function(value) {
		this.setState({ModalArsipIsOpen: value})
	},
	changeModalFileIsOpen: function(value) {
		this.setState({ModalFileIsOpen: value})
	},
	toggleModalCariIsOpen: function() {
		this.setState({ModalCariIsOpen: !this.state.ModalCariIsOpen})
	},
	toggleModalInputIsOpen: function() {
		this.setState({ModalInputIsOpen: !this.state.ModalInputIsOpen})
	},
  toggleModalEditIsOpen: function() {
		this.setState({ModalEditIsOpen: !this.state.ModalEditIsOpen})
	},
  toggleModalDisposisiIsOpen: function() {
    this.setState({ModalDisposisiIsOpen: !this.state.ModalDisposisiIsOpen})
  },
	toggleModalArsipIsOpen: function() {
		this.setState({ModalArsipIsOpen: !this.state.ModalArsipIsOpen})
	},
	toggleModalFileIsOpen: function() {
		this.setState({ModalFileIsOpen: !this.state.ModalFileIsOpen})
	},
	render: function(){
		return(
			React.createElement("div", null, 
				React.createElement(SideNav, {
					HeaderAccountName: this.state.HeaderAccountName, 
					HeaderAccountJabatan: this.state.HeaderAccountJabatan}), 
				React.createElement(Header, {
					HeaderAccountName: this.state.HeaderAccountName, 
					HeaderAccountJabatan: this.state.HeaderAccountJabatan}), 
				React.createElement(DataView, {
					changeSelectedDocSeries: this.changeSelectedDocSeries, 
					Singkatan: this.state.Singkatan, 
					SelectedDocSeries: this.state.SelectedDocSeries, 
					Data_DataView: this.state.Data_DataView}), 
				React.createElement(ModalCari, {
					getData: this.getData, 
					changeModalCariIsOpen: this.changeModalCariIsOpen, 
					ModalCariIsOpen: this.state.ModalCariIsOpen}), 
				React.createElement(ModalInput, {
          getData: this.getData, 
					changeModalInputIsOpen: this.changeModalInputIsOpen, 
          CurrentPage: this.state.CurrentPage, 
          Singkatan: this.state.Singkatan, 
					ModalInputIsOpen: this.state.ModalInputIsOpen}), 
        React.createElement(ModalEdit, {
          getData: this.getData, 
          changeModalEditIsOpen: this.changeModalEditIsOpen, 
          CurrentPage: this.state.CurrentPage, 
          SelectedDocSeries: this.state.SelectedDocSeries, 
          ModalEditIsOpen: this.state.ModalEditIsOpen}), 
        React.createElement(ModalDisposisi, {
          changeModalDisposisiIsOpen: this.changeModalDisposisiIsOpen, 
					getData: this.getData, 
					MyKodeJabatan: this.state.MyKodeJabatan, 
          ListBawahan: this.state.ListBawahan, 
          SelectedDocSeries: this.state.SelectedDocSeries, 
          ModalDisposisiIsOpen: this.state.ModalDisposisiIsOpen}), 
				React.createElement(ModalArsip, {
          changeModalArsipIsOpen: this.changeModalArsipIsOpen, 
					getData: this.getData, 
					CurrentPage: this.state.CurrentPage, 
					SelectedDocSeries: this.state.SelectedDocSeries, 
          ModalArsipIsOpen: this.state.ModalArsipIsOpen}), 
				React.createElement(ModalFile, {
          changeModalFileIsOpen: this.changeModalFileIsOpen, 
					getData: this.getData, 
					CurrentPage: this.state.CurrentPage, 
					SelectedDocSeries: this.state.SelectedDocSeries, 
          ModalFileIsOpen: this.state.ModalFileIsOpen}), 
				React.createElement("div", {className: "footer"}, 
					React.createElement("div", {className: "footer-section menu-section"}, 
						React.createElement(MenuCari, {
							SearchLength: this.state.SearchLength, 
							toggleModalCariIsOpen: this.toggleModalCariIsOpen}), 
						React.createElement(MenuInput, {
							toggleModalInputIsOpen: this.toggleModalInputIsOpen}), 
						React.createElement(MenuPrint, {
							SelectedDocSeries: this.state.SelectedDocSeries}), 
						React.createElement(MenuEdit, {
              toggleModalEditIsOpen: this.toggleModalEditIsOpen, 
							SelectedDocSeries: this.state.SelectedDocSeries}), 
						React.createElement(MenuDisposisi, {
              toggleModalDisposisiIsOpen: this.toggleModalDisposisiIsOpen, 
							SelectedDocSeries: this.state.SelectedDocSeries}), 
						React.createElement(MenuArsip, {
							toggleModalArsipIsOpen: this.toggleModalArsipIsOpen, 
							SelectedDocSeries: this.state.SelectedDocSeries}), 
						React.createElement(MenuFile, {
							toggleModalFileIsOpen: this.toggleModalFileIsOpen, 
							SelectedDocSeries: this.state.SelectedDocSeries}), 
						React.createElement(MenuInfo, {
							SelectedDocSeries: this.state.SelectedDocSeries})
					), 
					React.createElement("div", {className: "footer-section data-view-infoselectedrow"}, 
						React.createElement(DataViewInfoSelectedRow, {
							SelectedDocSeries: this.state.SelectedDocSeries})
					), 
					React.createElement("div", {className: "footer-section data-view-count"}, 
						React.createElement(DataViewCount, {
							CurrentPage: this.state.CurrentPage, 
							DocsCount: this.state.DocsCount})
					), 
					React.createElement("div", {className: "footer-section data-view-navigator"}, 
						React.createElement(DataViewNavigator, {
							getData: this.getData, 
							changeCurrentPage: this.changeCurrentPage, 
							CurrentPage: this.state.CurrentPage})
					)
				)
			)
		)
	}
})

React.render(React.createElement(App, null), document.getElementById('react'));