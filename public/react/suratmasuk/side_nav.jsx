//
var SideNav = React.createClass({
  render: function(){
    return(
      <div>
        <ul id="slide-out" className="side-nav">
          <li>
            <div className="userView">
              <div className="background side-nav-banner">

              </div>
              <a href="#!user"><img className="circle" src="/images/super_mario.jpg"/></a>
              <a href="#!name"><span className="white-text name">{this.props.HeaderAccountName}</span></a>
              <a href="#!email"><span className="white-text email">{this.props.HeaderAccountJabatan}</span></a>
              </div>
          </li>
          <li><a href="#!"><i className="material-icons">drafts</i>Surat Keluar</a></li>
          <li><a href="#!"><i className="material-icons">child_care</i>Admin</a></li>
          <li><div className="divider"></div></li>
          <li><a className="subheader">Akun</a></li>
          <li><a className="waves-effect" href="/surat/logout">Keluar</a></li>
        </ul>
      </div>
    )
  }
})
