// ...
var MenuCari = React.createClass({
  render: function(){
    if(this.props.SearchLength > 0){
      var _class = "btn-floating waves-effect waves-light red accent-3 overfly"
    }else{
      var _class = "btn-floating waves-effect waves-light blue accent-3 overfly"
    }
    return(
      <a className={_class} onClick={this.props.toggleModalCariIsOpen}>
        <i className="material-icons left">search</i>
      </a>
    )
  }
})

var MenuInput = React.createClass({

  render: function(){
    return(
      <a className="btn-floating waves-effect waves-light blue accent-3 overfly" onClick={this.props.toggleModalInputIsOpen} style={{marginLeft: "5px"}}>
        <i className="material-icons left">add</i>
      </a>
    )
  }
})

var MenuPrint = React.createClass({
  onClick: function(){
    var obj  = {"UniqCHar": "123ABC", "Data": this.props.SelectedDocSeries}
    var _obj = JSON.stringify(obj)
    window.open(URL_suratmasukPDFGenerator + _obj, "new_window")
  },
  render: function(){
    if(this.props.SelectedDocSeries.length >= 1){
      var _class = "btn-floating waves-effect waves-light blue accent-3 overfly"
    }else{
      var _class = "hidden"
    }
    return(
      <a className={_class} style={{marginLeft: "5px"}} onClick={this.onClick}>
        <i className="material-icons left">print</i>
      </a>
    )
  }
})

var MenuEdit = React.createClass({

  render: function(){
    if(this.props.SelectedDocSeries.length == 1){
      var _class = "btn-floating waves-effect waves-light blue accent-3 overfly"
    }else{
      var _class = "hidden"
    }
    return(
      <a className={_class} style={{marginLeft: "5px"}} onClick={this.props.toggleModalEditIsOpen} >
        <i className="material-icons left">mode_edit</i>
      </a>
    )
  }
})

var MenuDisposisi = React.createClass({

  render: function(){
    if(this.props.SelectedDocSeries.length == 1){
      var _class = "btn-floating waves-effect waves-light blue accent-3 overfly"
    }else{
      var _class = "hidden"
    }
    return(
      <a className={_class} style={{marginLeft: "5px"}} onClick={this.props.toggleModalDisposisiIsOpen}>
        <i className="material-icons left">people</i>
      </a>
    )
  }
})

var MenuArsip = React.createClass({

  render: function(){
    if(this.props.SelectedDocSeries.length == 1){
      var _class = "btn-floating waves-effect waves-light blue accent-3 overfly"
    }else{
      var _class = "hidden"
    }
    return(
      <a className={_class} style={{marginLeft: "5px"}} onClick={this.props.toggleModalArsipIsOpen}>
        <i className="material-icons left">folder</i>
      </a>
    )
  }
})

var MenuFile = React.createClass({

  render: function(){
    if(this.props.SelectedDocSeries.length == 1){
      var _class = "btn-floating waves-effect waves-light blue accent-3 overfly"
    }else{
      var _class = "hidden"
    }
    return(
      <a className={_class} style={{marginLeft: "5px"}} onClick={this.props.toggleModalFileIsOpen}>
        <i className="material-icons left">attach_file</i>
      </a>
    )
  }
})

var MenuInfo = React.createClass({

  render: function(){
    if(this.props.SelectedDocSeries.length == 1){
      var _class = "btn-floating waves-effect waves-light blue accent-3 overfly"
    }else{
      var _class = "hidden"
    }
    return(
      <a className={_class} style={{marginLeft: "5px"}}>
        <i className="material-icons left">info_outline</i>
      </a>
    )
  }
})
