var DataViewInfoSelectedRow = React.createClass({displayName: "DataViewInfoSelectedRow",
  getInitialState: function(){
    return({
    })
  },
  render: function(){
    return(
      React.createElement("div", {className: "footer-section-inner"}, 
        React.createElement("div", null, "Terpilih: ", this.props.SelectedDocSeries.length)
      )
    )
  }
})

var DataViewCount = React.createClass({displayName: "DataViewCount",
    render: function() {
      var start = this.props.CurrentPage * 10 - 9
      var end   = this.props.CurrentPage * 10

      if (end >= this.props.DocsCount){
        end = this.props.DocsCount
      }
      return(
        React.createElement("div", {className: "footer-section-inner", style: {paddingRight: "10px"}}, 
          React.createElement("div", null, start, " - ", end, " dari ", this.props.DocsCount)
        )
      )
    }
})

var DataViewNavigator = React.createClass({displayName: "DataViewNavigator",
  changeCurrentPage: function(){
    var page = $("#current_page_number").val()
    this.props.changeCurrentPage(page)
  },
  componentDidUpdate: function(prevProps, prevState){
    // console.log(prevProps.CurrentPage, this.props.CurrentPage)
    // CurrentPage Changed
    if(prevProps.CurrentPage !== this.props.CurrentPage){
      this.props.getData()
    }
  },
  prevPage: function(){
    var page = this.props.CurrentPage - 1
    this.props.changeCurrentPage(page)
  },
  nextPage: function(){
    var page = this.props.CurrentPage + 1
    this.props.changeCurrentPage(page)
  },
  render: function(){
    return(
      React.createElement("div", {className: "footer-section-inner"}, 
        React.createElement("span", null, 
          React.createElement("i", {className: "material-icons navigator", onClick: this.prevPage}, "keyboard_arrow_left")
        ), 
        React.createElement("span", null, 
          React.createElement("input", {id: "current_page_number", maxLength: "2", type: "text", value: this.props.CurrentPage, onChange: this.changeCurrentPage})
        ), 
        React.createElement("span", null, 
          React.createElement("i", {className: "material-icons navigator", onClick: this.nextPage}, "keyboard_arrow_right")
        )
      )
    )
  }
})