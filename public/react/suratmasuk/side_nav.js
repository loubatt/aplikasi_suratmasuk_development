var SideNav = React.createClass({displayName: "SideNav",
  render: function(){
    return(
      React.createElement("div", null, 
        React.createElement("ul", {id: "slide-out", className: "side-nav"}, 
          React.createElement("li", null, 
            React.createElement("div", {className: "userView"}, 
              React.createElement("div", {className: "background side-nav-banner"}

              ), 
              React.createElement("a", {href: "#!user"}, React.createElement("img", {className: "circle", src: "/images/super_mario.jpg"})), 
              React.createElement("a", {href: "#!name"}, React.createElement("span", {className: "white-text name"}, this.props.HeaderAccountName)), 
              React.createElement("a", {href: "#!email"}, React.createElement("span", {className: "white-text email"}, this.props.HeaderAccountJabatan))
              )
          ), 
          React.createElement("li", null, React.createElement("a", {href: "#!"}, React.createElement("i", {className: "material-icons"}, "drafts"), "Surat Keluar")), 
          React.createElement("li", null, React.createElement("a", {href: "#!"}, React.createElement("i", {className: "material-icons"}, "child_care"), "Admin")), 
          React.createElement("li", null, React.createElement("div", {className: "divider"})), 
          React.createElement("li", null, React.createElement("a", {className: "subheader"}, "Akun")), 
          React.createElement("li", null, React.createElement("a", {className: "waves-effect", href: "/surat/logout"}, "Keluar"))
        )
      )
    )
  }
})