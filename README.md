## Aplikasi Surat Masuk & Keluar ##
Pengadministrasian surat masuk dan keluar berbasis web dengan basis data No SQL (JSON Object; MongoDB).

### Spesifikasi ###
- Type      : Web Based
- Front End : ReactJS, Materialize css
- Back End  : Golang
- Database  : MongoDB

### Fitur ###
- Tracking surat
- Menu tambah surat
- Menu edit surat
- Menu cari surat
- Menu disposisi surat
- Menu cetak disposisi
- Menu upload file
- Menu arsip surat
- Menu lihat detail surat (versi2 on progress)
- Surat Keluar (versi2 on progress)

## Screenshoot : ##

**Navigation Bar**

![Untitled.png](https://bitbucket.org/repo/pEd6Ej/images/1187861630-Untitled.png)

**Pencarian**

![overview1.png](https://bitbucket.org/repo/pEd6Ej/images/2864129527-overview1.png)

**Add File**

![Untitled2.png](https://bitbucket.org/repo/pEd6Ej/images/3075315282-Untitled2.png)