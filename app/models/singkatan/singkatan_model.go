package singkatan_model

import(
  "net/http"
	"gopkg.in/mgo.v2/bson"
	"../../models"
  "../../session"
)

// GET DATA
func List(r *http.Request) (interface{}, []bson.M) {
	var result []bson.M
  var selectField bson.M
  selectField = bson.M{"_id": 0, "kode_jabatan": 0, "rule_name": 0}
  kode_jabatan  := session.Get(r, "kode_jabatan")
	err := models.MongoDB.C("singkatan").Find(bson.M{"kode_jabatan": kode_jabatan}).Select(selectField).Sort("-singkatan").All(&result)
  return err, result
}
