package report_model

import(
	// "fmt"
	// "time"
	// "strconv"
	// "strings"
    "net/http"
    // "github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"

	// "../../session"
	// "../../function"
	"../../models"
	// "../../models/nomor_agenda"
	// "../../models/wordrank"
	// "../../models/jabatan"
	// "../../models/user"
)

func One(r *http.Request) interface{} {
	
	var result bson.M
	err := models.MongoDB.C("MR222USER_kodejabatanNamapegawai").Find(bson.M{}).One(&result)
	if err != nil {
		return err
	}else{
		return result
	}
}