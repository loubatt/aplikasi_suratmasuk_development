package report

import(
    // "os"
    // "io"
	"fmt"
    "net/http"
    // "html/template"
    "encoding/json"
    // "github.com/gorilla/mux"
    // "../../config"
	// "../../auth"
    // "../../session"
	// "../../function"
    "../../models/report"
    // "../../models/wordrank"
)

type Response map[string]interface{}

func (r Response) String() (s string) {
    b, err := json.Marshal(r)
    if err != nil {
            s = ""
            return
    }
    s = string(b)
    return
}

func ListUser(w http.ResponseWriter, r *http.Request) {
    // Public Data, tanpa Auth
    datauser := report_model.One(r)

    finalResult := Response{"users": datauser}
    w.Header().Set("Content-Type", "application/json")
    fmt.Fprint(w, finalResult)
    return      
}
