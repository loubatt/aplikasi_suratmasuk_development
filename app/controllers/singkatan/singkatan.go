package singkatan

import(
  "fmt"
  "net/http"
  "encoding/json"
  "../../auth"
  "../../models/singkatan"
)

type Response map[string]interface{}

func (r Response) String() (s string) {
    b, err := json.Marshal(r)
    if err != nil {
            s = ""
            return
    }
    s = string(b)
    return
}

func List(w http.ResponseWriter, r *http.Request) {
    if auth.IsUser(r) == "" {
      http.Redirect(w, r, "/", 301)
    }else{

      var result interface{}
      error, list := singkatan_model.List(r)

      if len(list) == 0 {
        result = []string{}
      }else{
        result = list[0]
      }

      output := Response{
          "list"  : result,
          "error" : error,
      }
      w.Header().Set("Content-Type", "application/json")
      fmt.Fprint(w, output)
      return

    }
}
