package route

import(
    "os"
    "net/http"
    "github.com/gorilla/mux"

    "../config"
    "../controllers/admin"
    "../controllers/root"
    "../controllers/login"
    "../controllers/suratmasuk"
    "../controllers/suratkeluar"
    "../controllers/jabatan"
    "../controllers/arsip"
    "../controllers/report"
    "../controllers/pdfgenerator"
    "../controllers/singkatan"

    // DEVELOPMENT PURPOSE ONLY, REMOVE WHEN PRODUCTION READY
    "../session"
)

type justFilesFilesystem struct {
    fs http.FileSystem
}

func (fs justFilesFilesystem) Open(name string) (http.File, error) {
    f, err := fs.fs.Open(name)
    if err != nil {
        return nil, err
    }
    return neuteredReaddirFile{f}, nil
}

type neuteredReaddirFile struct {
    http.File
}

func (f neuteredReaddirFile) Readdir(count int) ([]os.FileInfo, error) {
    return nil, nil
}

func Route() {
    AppPath := "/" + config.GetConfig("AppName")
    mx      := mux.NewRouter()

    // Root
    mx.HandleFunc(AppPath + "/", root.Index).Methods("GET")

    // ### ADMIN ###
    mx.HandleFunc(AppPath + "/admin", admin.Home).Methods("GET")
    mx.HandleFunc(AppPath + "/admin/login", admin.Login).Methods("GET")
    mx.HandleFunc(AppPath + "/admin/login", admin.LoginPost).Methods("POST")
    mx.HandleFunc(AppPath + "/admin/logout", admin.Logout).Methods("GET")
    mx.HandleFunc(AppPath + "/admin/user", admin.User).Methods("GET")
    mx.HandleFunc(AppPath + "/admin/user/add", admin.UserAdd).Methods("GET")
    mx.HandleFunc(AppPath + "/admin/user/add", admin.UserAddPost).Methods("POST")
    mx.HandleFunc(AppPath + "/admin/user/edit/{id_user}", admin.UserEdit).Methods("GET")
    mx.HandleFunc(AppPath + "/admin/user/edit/{id_user}", admin.UserEditPost).Methods("POST")
    mx.HandleFunc(AppPath + "/admin/user/remove/{id_user}", admin.UserRemove).Methods("GET")
    mx.HandleFunc(AppPath + "/admin/user/remove/{id_user}", admin.UserRemovePost).Methods("POST")
    mx.HandleFunc(AppPath + "/admin/jabatan", admin.Jabatan).Methods("GET")
    mx.HandleFunc(AppPath + "/admin/jabatan/badge", admin.Jabatan).Methods("GET")
    mx.HandleFunc(AppPath + "/admin/jabatan/add", admin.JabatanAdd).Methods("GET")
    mx.HandleFunc(AppPath + "/admin/jabatan/add", admin.JabatanAddPost).Methods("POST")
    mx.HandleFunc(AppPath + "/admin/jabatan/edit/{id_jabatan}", admin.JabatanEdit).Methods("GET")
    mx.HandleFunc(AppPath + "/admin/jabatan/edit/{id_jabatan}", admin.JabatanEditPost).Methods("POST")
    mx.HandleFunc(AppPath + "/admin/jabatan/remove/{id_jabatan}", admin.JabatanRemove).Methods("GET")
    mx.HandleFunc(AppPath + "/admin/jabatan/remove/{id_jabatan}", admin.JabatanRemovePost).Methods("POST")
    mx.HandleFunc(AppPath + "/admin/form_search", admin.FormSearch).Methods("GET")

    // USER
    mx.HandleFunc(AppPath + "/login", login.Index).Methods("GET")
    mx.HandleFunc(AppPath + "/login", login.Post).Methods("POST")
    mx.HandleFunc(AppPath + "/logout", login.Logout).Methods("GET")

    // Surat Masuk
    mx.HandleFunc(AppPath + "/suratmasuk", suratmasuk.Index).Methods("GET")
    mx.HandleFunc(AppPath + "/suratmasuk/", suratmasuk.Index).Methods("GET")
    mx.HandleFunc(AppPath + "/suratmasuk/suggest/{word}", suratmasuk.Suggest).Methods("GET")
    mx.HandleFunc(AppPath + "/suratmasuk/page/{page}", suratmasuk.Page).Methods("GET")
    mx.HandleFunc(AppPath + "/suratmasuk/printpreview", suratmasuk.PrintPreview).Methods("GET")
    mx.HandleFunc(AppPath + "/suratmasuk/printpreview/data", suratmasuk.PrintPreviewData).Methods("GET")
    mx.HandleFunc(AppPath + "/suratmasuk/search", suratmasuk.Search).Methods("POST")
    mx.HandleFunc(AppPath + "/suratmasuk/search/getvalue", suratmasuk.SearchGetValue).Methods("GET")
    mx.HandleFunc(AppPath + "/suratmasuk/search/clear", suratmasuk.SearchClear).Methods("GET")
    mx.HandleFunc(AppPath + "/suratmasuk/search/form/getvars", suratmasuk.FormGetVars).Methods("GET")
    mx.HandleFunc(AppPath + "/suratmasuk/detail/{doc_series}", suratmasuk.Detail).Methods("GET")
    mx.HandleFunc(AppPath + "/suratmasuk/detail/for_edit/{doc_series}", suratmasuk.DetailForEdit).Methods("GET")
    mx.HandleFunc(AppPath + "/suratmasuk/detail/for_disposisi/{doc_series}", suratmasuk.DetailForDisposisi).Methods("GET")
    mx.HandleFunc(AppPath + "/suratmasuk/detail/for_arsip/{doc_series}", suratmasuk.DetailForArsip).Methods("GET")
    mx.HandleFunc(AppPath + "/suratmasuk/input", suratmasuk.Input).Methods("POST")
    mx.HandleFunc(AppPath + "/suratmasuk/edit/{doc_series}", suratmasuk.Edit).Methods("POST")
    mx.HandleFunc(AppPath + "/suratmasuk/file/upload/{doc_series}", suratmasuk.Upload).Methods("POST")
    mx.HandleFunc(AppPath + "/suratmasuk/file/list/{doc_series}", suratmasuk.DetailForFile).Methods("GET")
    mx.HandleFunc(AppPath + "/suratmasuk/file/remove/{file_name}", suratmasuk.RemoveFile).Methods("GET")
    mx.HandleFunc(AppPath + "/suratmasuk/pic/add/{doc_series}", suratmasuk.PicPost).Methods("POST")
    mx.HandleFunc(AppPath + "/suratmasuk/pic/remove/{doc_series}", suratmasuk.PicRemove).Methods("POST")
    mx.HandleFunc(AppPath + "/suratmasuk/settanggalterima", suratmasuk.SetTanggalTerima).Methods("POST")
    mx.HandleFunc(AppPath + "/jabatan/list", jabatan.List).Methods("GET")
    mx.HandleFunc(AppPath + "/jabatan/bawahan", jabatan.ListBawahan).Methods("GET")

    // Arsip
    mx.HandleFunc(AppPath + "/arsip/{doc_series}", arsip.Get).Methods("GET")
    mx.HandleFunc(AppPath + "/arsip/{sms_or_skl}/{doc_series}", arsip.Post).Methods("POST")
    mx.HandleFunc(AppPath + "/arsip/remove/{sms_or_skl}/{doc_series}", arsip.Remove).Methods("POST")
    mx.HandleFunc(AppPath + "/arsip/kode/{kode_arsip}", arsip.ByKodeList).Methods("GET")
    mx.HandleFunc(AppPath + "/arsip/keterangan/{keterangan_arsip}", arsip.ByKetList).Methods("GET")
    mx.HandleFunc(AppPath + "/arsip/kodeorketerangan/{value}", arsip.ByKetOrKodeList).Methods("GET")

    // Surat Keluar
    mx.HandleFunc(AppPath + "/suratkeluar", suratkeluar.Index).Methods("GET")
    mx.HandleFunc(AppPath + "/suratkeluar/", suratkeluar.Index).Methods("GET")
    mx.HandleFunc(AppPath + "/suratkeluar/input", suratkeluar.Input).Methods("POST")
    mx.HandleFunc(AppPath + "/suratkeluar/page/{page}", suratkeluar.Page).Methods("GET")
    mx.HandleFunc(AppPath + "/suratkeluar/upload/{doc_series}", suratkeluar.Upload).Methods("POST")
    mx.HandleFunc(AppPath + "/suratkeluar/detail/{doc_series}", suratkeluar.Detail).Methods("GET")
    mx.HandleFunc(AppPath + "/suratkeluar/edit/{doc_series}", suratkeluar.Edit).Methods("POST")
    mx.HandleFunc(AppPath + "/suratkeluar/check_last", suratkeluar.CheckLast).Methods("POST")
    mx.HandleFunc(AppPath + "/suratkeluar/search", suratkeluar.Search).Methods("POST")
    mx.HandleFunc(AppPath + "/suratkeluar/search/getvalue", suratkeluar.SearchGetValue).Methods("GET")
    mx.HandleFunc(AppPath + "/suratkeluar/search/clear", suratkeluar.SearchClear).Methods("GET")

    // Report
    mx.HandleFunc(AppPath + "/report/list_user", report.ListUser).Methods("GET")
    mx.HandleFunc(AppPath + "/singkatan", singkatan.List).Methods("GET")

    // PDF
    mx.HandleFunc(AppPath + "/suratmasuk/pdf/{doc_serieses}", pdfgenerator.PDFcreate).Methods("GET")

    // DEVELOPMENT PURPOSE ONLY, REMOVE WHEN PRODUSTION READY
    mx.HandleFunc(AppPath + "/session", session.Show).Methods("GET")

    // Public
    fs := justFilesFilesystem{http.Dir(config.GetConfig("PublicPath"))}
    mx.PathPrefix("/").Handler(http.StripPrefix("/", http.FileServer(fs)))

    http.ListenAndServe(":" + config.GetConfig("Port"), mx)
}
